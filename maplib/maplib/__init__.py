"""
global wrapper for every service of MAP. 

This init allows more readable imports from 3rd party modules,
like maplib.Vault() instead of maplib.mapvault.Vault().

You can request the version of maplib with `maplib.__version__`.
"""
__version__ = "0.9.8"
from .internals import BreakerThread
from .mapinflux import Influx
from .maplog import Logger
from .maploki import Loki
from .mapminio import Minio
from .mapnetbox import Netbox
from .mapvault import Vault, privileged
from .maphasura import Hasura
from .mapgrafana import Grafana
