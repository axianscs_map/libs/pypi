"""Module that creates an easier interface to netbox

The simplest usage is `netbox = maplib.Netbox()`.

From there, you can requests devices with a given tag using `.retrieve_hosts_with_tags(tag)`

Your microservice can also change the status of the device in netbox using `.update_device_status(hostname,status)`

Environment variables:

- `MAPLIB_NETBOX_URL`: specify a different complete url for netbox.

"""
import json
import os
from datetime import date

import pynetbox
import requests

#from .internals import deprecated
from .maplog import Logger
from .mapvault import Vault

requests.urllib3.disable_warnings()


class MyEncoder(json.JSONEncoder):
    """Json serializer to update comment in netbox
    """

    def default(self, obj):
        if isinstance(obj, NetboxComment):
            return vars(obj)
        return json.JSONEncoder.default(self, obj)


class NetboxComment():
    """Message for netbox
    """

    def __init__(self, message_comment=None):
        """Create a NetboxMessage object.

        Args:
            message_comment (dict, optional): should be represented like so: 
                                            {
                                                "error_log" : ['msg', 'msg2'] - > limit -> 30
                                                "warranty_contract" : 
                                                {
                                                        "is_covered": "",
                                                        "coverage_end_date": "",
                                                        "sr_no": "",
                                                        "parent_sr_no": "",
                                                        "contract_site_address1": "",
                                                        "service_line_descr": "",
                                                        "base_pid_list": "",
                                                        "EndOfSaleDate": "",
                                                        "warranty_type": "",
                                                        "warranty_end_date": "",
                                                        "product_description": "",
                                                        "EndOfSWMaintenanceReleases" : "",
                                                        "EndOfSecurityVulSupportDate" : "",
                                                        "EndOfServiceContractRenewal" : "",
                                                        "LastDateOfSupport": ""
                                                }
                                            }. Defaults to None.
        """
        if message_comment:
            self.message_comment = message_comment
        else:
            self.message_comment = {
                "error_log": [],
                "warranty_contract": {}
            }

    def update_comment_log(self, comment):
        """Add a comment to the rotating list.

        Args:
            comment (str): the message to add to the list
        """
        if len(self.message_comment['error_log']) >= 10:
            self.message_comment['error_log'].pop()
        self.message_comment['error_log'].insert(0, comment)

    def update_comment_warranty(self, warranty):
        """update the section relative to the warranty in the device comments
        """
        self.message_comment["warranty_contract"] = warranty


def comment_instance(device):
    """ Update waranty information of device
    """
    try:
        device_comment = json.loads(device.comments)
        if isinstance(NetboxComment(**device_comment), NetboxComment):
            return NetboxComment(**device_comment)
        return False
    except Exception as e:
        print(e)
        return False


class Netbox():
    """netbox API object
    """

    def __init__(self, autoconfig=True, url="netbox", token=""):
        """creates a Netbox() instance

        Args:
            autoconfig (bool, optional): set to false if you configure using args. Defaults to True.
            url (str, optional): the url of netbox. Defaults to "netbox".
            token (str, optional): the token for netbox. Defaults to "".
        """
        self.logger = Logger("netbox")
        if "MAPLIB_NETBOX_URL" in os.environ:
            # this should be a complete url, with schema.
            self.url = os.getenv("MAPLIB_NETBOX_URL")
        elif not url.startswith("http://") and not url.startswith("https://"):
            self.url = "http://"+url
        else:
            self.url = url

        if autoconfig:
            v = Vault()
            if v.authenticate():
                secret = v.get_netbox_secrets()
                self.token = secret["token"]
            else:
                self.logger.critical(
                    "autoconfig is on, but vault is unreacheable")
        else:
            self.token = token
        self.nb = None

        # lambdas Getter
        self.get_full_dict = lambda x: dict(self.nb.dcim.devices.get(name=x))
        self.get_manufacturers = lambda: {x['name']: x['id'] for x in [
            dict(x) for x in self.nb.dcim.manufacturers.all()]}
        self.get_models = lambda: {x['model']: x['id'] for x in [
            dict(x) for x in self.nb.dcim.device_types.all()]}
        self.get_ips = lambda: {x['address']: x['id'] for x in [
            dict(x) for x in self.nb.ipam.ip_addresses.all()]}
        self.get_interface_for_device = lambda device_name, interface_name: {x['name']: x for x in [
            dict(x) for x in self.nb.dcim.interfaces.get(device=device_name, name=interface_name)]}

        # lambda setters (create)
        self.create_manufacturer = lambda manufacturer_name: dict(self.nb.dcim.manufacturers.get(
            name=self.nb.dcim.manufacturers.create(name=manufacturer_name, slug=manufacturer_name)))['id']

        # autoconnect
        self.nb = pynetbox.api(self.url, self.token)
        if "https" in self.url:
            self.nb.http_session.verify = False

        # test
        try:
            self.nb.version
        except Exception as error:
            self.logger.error("could not connect to netbox : {}".format(error))

    def retrieve_all_with_tags(self, tag):
        """Wrapper that combine `self.retrieve_hosts_with_tags()` and `self.retrieve_vms_with_tags()`

        Args:
            tag (str): the tag to filter with

        Returns:
            dict: a dict of all devices & VMs that match the tag. Note that if a VM has the same name as a device, the latter will be overwritten by the former in the dict returned by this method
        """
        devs = self.retrieve_hosts_with_tags(tag)
        vms = self.retrieve_vms_with_tags(tag)
        devs.update(vms)
        return devs

    def retrieve_hosts_with_tags(self, tag):
        """get the infos for each device in netbox who has a given tag

        Args:
            tag (str): the tag to filter devices

        Returns:
            dict: a dict of devices of type {'devicename':{'ip':"xx","tags":[]}}
        """
        hosts = {}
        for dev in self.nb.dcim.devices.filter(tag=tag):
            dev_dict = dict(dev)
            try:
                hosts[dev] = {'name': dev, "id": dev_dict["id"], "site": dev_dict["site"]["name"], "manufacturer": dev_dict["device_type"]
                              ['manufacturer']['slug'], 'tags': dev_dict['tags'], 'parsed_tags': parse_tags(dev_dict['tags'])}

                try:
                    hosts[dev]['ip'] = dev_dict['primary_ip']["address"].split(
                        "/")[0]
                except Exception:
                    self.logger.warning(
                        "The device %s does not have an IP assigned", dev)

                # add the serial number if it exists, None otherwise
                try:
                    if dev_dict['serial'] == '':
                        hosts[dev]["serial_number"] = None
                    else:
                        hosts[dev]["serial_number"] = dev_dict['serial']

                except Exception:
                    pass

            except Exception as error:
                self.logger.error(
                    "Unable to parse data for device %s : %s", dev, error)
        return hosts

    def retrieve_vms_with_tags(self, tag):
        """Retrieve all the VMs that have a given tag

        Args:
            tag (str): the tag to filter vms with

        Returns:
            dict: a dict of all matching VMs
        """
        vms = {}
        for vm in self.nb.virtualization.virtual_machines.filter(tag=tag):
            vm_dict = dict(vm)
            try:
                vms[vm] = {'name': vm, "id": vm_dict["id"], "site": vm_dict["site"]["name"], "cluster": vm_dict["cluster"]
                           ['name'], 'tags': vm_dict['tags'], 'parsed_tags': parse_tags(vm_dict['tags'])}

                try:
                    vms[vm]['ip'] = vm_dict['primary_ip']["address"].split(
                        "/")[0]
                except Exception:
                    self.logger.warning(
                        "The device %s does not have an IP assigned", vm)

            except Exception as error:
                self.logger.error(
                    "Unable to parse data for device %s : %s", vm, error)
        return vms

    def update_device_status(self, hostname, status):
        """Update a device status (by its hostname) 

        Args:
            hostname (str): the name of the device to be updated
            status (str): the new status of the device, can be one from ['FAILED', 'ACTIVE', 'OFFLINE','PLANNED', 'STAGED', 'INVENTORY', 'DECOMMISSIONING']

        Returns:
            bool: True if ok, False otherwise
        """
        avail_status = ['FAILED', 'ACTIVE', 'OFFLINE',
                        'PLANNED', 'STAGED', 'INVENTORY', 'DECOMMISSIONING']
        try:
            device = self.find_device_by_name(hostname)[0]
            if status.upper() in avail_status and device.status.value != status:
                device.status = status
                device.save()
                self.logger.info("Status of {0} updated".format(hostname))
            else:
                self.logger.error(
                    "This status is not available, please select one from Netbox.")
                return False
        except Exception as error:
            self.logger.error("could not parse data : %s", error)
            return False

        return True

    def add_comment_to_device(self, hostname, message):
        """ Add commentary to a device
            Will look if comment already existe
            if not create an instance of netbox comment

            TO DO : Choose the message type; Must be sucess, error, or info; 
        """
        device = self.find_device_by_name(hostname)[0]
        comment = comment_instance(device)
        # check if previous comment instance is compatable
        if comment:
            comment.update_comment_log(
                {"date": str(date.today()), "error": message})

        # if not create a new one
        else:
            comment = NetboxComment()
            comment.update_comment_log(
                {"date": str(date.today()), "error": message})
        device.comments = json.dumps(comment, cls=MyEncoder)
        device.save()

    def update_warranty(self, sn,  warranty_information):
        """update the warranty information of a given device (by its serial number)

        Args:
            sn (str): the serial of the device
            warranty_information (str): the update warranty status

        Returns:
            bool: True if it succeeded, False otherwise
        """
        # check if warranty is compatable to update warranty information of device
        # try:
        #    json.loads(warranty_information)
        # except ValueError as e:
        #    self.logger.error("Impossible to parse warranty information")
        #    self.logger.error(e)
        #    return False
        device = self.find_device_by_serial_number(sn)[0]
        # check if previous comment instance is compatable
        comment = comment_instance(device)
        if comment:
            comment.update_comment_warranty(warranty_information)
        # if not create a new one
        else:
            comment = NetboxComment()
            comment.update_comment_warranty(warranty_information)

        device.comments = json.dumps(comment, cls=MyEncoder)
        try:
            device.save()
            return True
        except Exception as e:
            self.logger.error("Could not save the warranty : {}".format(e))
            return False

    def find_device_by_name(self, hostname):
        """Return devices using their names

        Args:
            hostname (str): the name of the devices

        Returns:
            list: list of devices with the given name
        """
        try:
            list_target = self.nb.dcim.devices.filter(name=hostname)
        except Exception:
            list_target = None
        return list_target

    def find_device_by_serial_number(self, serial_number):
        """return a device filtered by its serial

        Args:
            serial_number (str): the serial number

        Returns:
            list: list of devices matching
        """
        try:
            list_target = self.nb.dcim.devices.filter(serial=serial_number)
        except Exception:
            list_target = None
        return list_target


def parse_tags(list_of_tags):
    """parse the tags from netbox by splitting by :

    Args:
        list_of_tags (list): the list of tags from netbox

    Returns:
        dict: a k:v dict of the tags splitted by : 
    """
    return {k: v for (k, v) in [tuple(tag.split(":", 1)) for tag in [t for t in list_of_tags if ":" in t]]}
