"""Module for standardized logging

Create a logger with 
```
my_log = maplib.Logger("my_app")
```

It supports methods like a regular Logging instance :

- .debug()
- .info()
- .warning()
- .error()
- .critical()
- .exception()


Environment Variables:

- `MAPLIB_DEBUG`: This will set the level to DEBUG for every module. This might be useful when debbuging. Set it to any string to enable it.

"""
import logging
import os

if isinstance(os.getenv("SENTRY_DSN"), str):
    try:
        import sentry_sdk
        from sentry_sdk.integrations.logging import LoggingIntegration
    except ImportError:
        pass


def Logger(name='app', level='INFO', sentry=False):
    """Creates a logger object that can be used to format log output
    It also sets up sentry error reporting for levels above INFO.

    Args:
        name (str, optional): the name of the app using the module. 
        If using a per-target logger, set the name to app_name-target-ip. Defaults to 'app'.
        level (str, optional): Level of the messages to log. 
        Can be DEBUG,INFO,WARNING,ERROR,CRITICAL. Defaults to 'INFO'.

    Returns:
        logging.Logger: A logger object
    """
    # Create a custom logger
    log = logging.getLogger(name)

    # as we might be in a threaded environment, we need to make sure that we do not add a handler
    # every time we spawn a new thread with a logger object.
    log.propagate = False
    if log.hasHandlers():
        return log

    # add a streamhandler for stdout
    stdout_handler = logging.StreamHandler()

    # Format it
    stdout_format = logging.Formatter(
        '%(asctime)s :: %(name)s :: %(levelname)s :: %(message)s', "%Y-%m-%d %H:%M:%S")
    stdout_handler.setFormatter(stdout_format)
    log.addHandler(stdout_handler)

    # add the default debug level, if MAPLIB_DEBUG is present then force DEBUG
    if os.getenv("MAPLIB_DEBUG") is not None:
        log.setLevel("DEBUG")
    else:
        log.setLevel(level)

    # If enabled, setup sentry error uploading
    if sentry or isinstance(os.getenv("SENTRY_DSN"), str):
        try:

            sentry_logging = LoggingIntegration(
                level=logging.WARNING,
                event_level=logging.ERROR
            )
            sentry_sdk.init(os.getenv("SENTRY_DSN"),
                            integrations=[sentry_logging])
            log.debug("sentry logging enabled")
        except Exception as err:
            log.error("Something failed when configuring sentry : %s", err)

    # return the logger object
    return log
