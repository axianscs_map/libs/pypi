"""
internal functions and utils for maplib
"""
import warnings
import threading
from functools import lru_cache, wraps
from datetime import datetime, timedelta


def deprecated(message):
    """
    decorator for deprecated functions
    """
    def deprecated_decorator(func):
        def deprecated_func(*args, **kwargs):
            warnings.warn("{} is a deprecated function. {}".format(func.__name__, message),
                          category=DeprecationWarning,
                          stacklevel=2)
            warnings.simplefilter('default', DeprecationWarning)
            return func(*args, **kwargs)
        return deprecated_func
    return deprecated_decorator


def time_cache(seconds: int = 120, maxsize: int = 32):
    """a time senstitive version of @lru_cache. Cache expires after `seconds`.

    Args:
        seconds (int, optional): Lifetime of the cache entry, in seconds. Defaults to 120.
        maxsize (int, optional): Max number of entry in the lru_cache. Defaults to 32.
    """
    def wrapper_cache(func):
        func = lru_cache(maxsize=maxsize)(func)
        func.expire = datetime.utcnow() + timedelta(seconds=seconds)

        @wraps(func)
        def wrapped_func(*args, **kwargs):
            if datetime.utcnow() >= func.expire:
                func.cache_clear()
                func.expire = datetime.utcnow() + timedelta(seconds=seconds)

            return func(*args, **kwargs)

        return wrapped_func

    return wrapper_cache


class BreakerThread(threading.Thread):
    """Overwrite of the threading.Thread class
    adds a threading.Event object for signal passing between threads.

    Args:
        threading (threading.Thread): the thread to create
    """

    def __init__(self,  *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._stop_event = threading.Event()
        self.processing_lock = threading.Lock()

    def stop(self):
        """sets the Event so the thread can be notified
        """
        self._stop_event.set()

    def stopped(self):
        """returns the state of the Event

        Returns:
            bool: True if set, False otherwise
        """
        return self._stop_event.is_set()
