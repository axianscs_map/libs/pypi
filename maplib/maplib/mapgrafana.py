"""module for listing, creating and deleting dashboards on grafana

You can overwrite each arguments from the Grafana() constructor using the following env vars:

- `MAPLIB_GRAFANA_URL`: full url (with proto & port) to grafana
- `MAPLIB_GRAFANA_USERNAME`: username
- `MAPLIB_GRAFANA_PASSWORD`: password

If you using this module inside a loop, for example to add a dashboard when a new target appeared, or during some loop logic, use
`cached_dashboard_exists()` instead of `dashboard_exists()`. The former is a wrapper around the latter with an integrated time-sensitive cache,
so that we don't overload grafana. The cache expire every 120s.
"""
import os

import requests

from .internals import time_cache
from .maplog import Logger
from .mapvault import Vault


class Grafana():
    """Instance for grafana
    """

    def __init__(self, autoconfig=True, url="http://grafana:3000", username=None, password=None):
        """Creates an instance based on the parameters given
        By default, it will use Vault to retrieve the credentials. 
        You can overwrite that by setting autoconfig to False, and passing the arguments.
        Finally you can use env vars to set these settings.

        The priority order is (in increasing order): autoconfig < arguments < env var. That means that env vars overwrite anything.
        Args:
            autoconfig (bool, optional): Set to True to use vault as the credentials provider, else False. Defaults to True.
            url (str, optional): The url for grafana. Defaults to "http://grafana:3000".
            username (str, optional): user for authenticate to grafana. Defaults to None.
            password (str, optional): password for grafana. Defaults to None.
        """
        self.logger = Logger("grafana")
        if autoconfig:
            v = Vault()
            s = v.read_secret("services/grafana/m2m")
            if s:
                self.url = url
                self.username = s["user"]
                self.password = s["password"]
            else:
                self.logger.critical(
                    "autoconfig is on, but no secrets were found for grafana !")

        # not auto
        else:
            self.url = url
            self.username = username
            self.password = password

        # overwrite if env vars
        for local_var, env_var in [("url","MAPLIB_GRAFANA_URL"),("username","MAPLIB_GRAFANA_USERNAME"),("password","MAPLIB_GRAFANA_PASSWORD")]:
            if env_var in os.environ.keys():
                self.__setattr__(local_var,os.environ[env_var])

        self.session = requests.Session()
        self.session.auth = (self.username, self.password)

    def get(self, url: str) -> dict:
        """wrapper around requests.get that adds the proper authentification

        Args:
            url (str): relative URL for the request. Should begin with `/api/xxx`

        Returns:
            dict: the json representation of the returned data, or None if the request failed
        """
        try:
            self.logger.debug("making a GET to %s", url)
            r = self.session.get(self.url+url)
        except requests.exceptions.RequestException as http_err:
            self.logger.error(
                "We couldn't access grafana at %s, error is %s", self.url, http_err)
            return None
        if not r.ok:
            self.logger.error(
                "Grafana returned a %s, because %s", r.status_code, r.text)
            return None
        self.logger.debug("%s returned len %s", url, len(r.text))
        return r.json()

    def list_dashboards(self, folderId=None) -> dict:
        """lists dahsboard in a given folder (or all by defaults)

        Args:
            folderId (int, optional): In which folder to list the dashboards. Defaults to None.

        Returns:
            dict: a {dashboardName:{dashboadInfo}} dict, or None if the query failed
        """
        query = "/api/search?type=dash-db"
        if folderId:
            query += "&folderIds={}".format(folderId)

        r = self.get(query)
        if r:
            return {d.pop("title"): d for d in r}

    @time_cache(seconds=60)
    def cached_dashboard_exists(self, name: str) -> bool:
        """wrapper around `dashboard_exists` with a time cache of 60s.
        This is meant to be used in a microservice main loop, while keeping the number of calls to grafana API to a minimum.

        Args:
            name (str): name of the dashboard to check for

        Returns:
            bool: True if the dashboard exists, False otherwise
        """
        return self.dashboard_exists(name)

    def dashboard_exists(self, name: str) -> bool:
        """returns simply if the dashboard already exists

        Args:
            name (str): name of the dashboard

        Returns:
            bool: True if exists, False otherwise
        """
        return name in self.list_dashboards()

    def delete_dashboard(self, name: str) -> bool:
        """delete a dashboard by its name

        Args:
            name (str): name of the dashboard

        Returns:
            bool: True if successful, False otherwise
        """
        if self.dashboard_exists(name):
            dashUid = self.list_dashboards()[name]["uid"]
            r = self.session.delete(
                self.url+"/api/dashboards/uid/{}".format(dashUid))
            if not r.ok:
                self.logger.warning(
                    "Dashboard could not be deleted : {},{}".format(r.status_code, r.text))
                return False

            return True

    def list_folders(self) -> dict:
        """list the folders available

        Returns:
            dict: a {foldername:{folderinfo}} dict, or None if the query failed
        """
        r = self.get("/api/search?type=dash-folder")
        if r:
            return {d.pop("title"): d for d in r}

    def create_folder(self, name) -> dict:
        raise NotImplementedError

    def delete_folder(self, name) -> dict:
        raise NotImplementedError

    def push_dashboard(self, dashboardJson, overwrite=False, folderId=None, message=None) -> bool:
        """Push a given dict (a dashboard JSON model) to grafana. 

        Args:
            dashboardJson (dict): the dashboard model to add/update
            overwrite (bool, optional): Set to True if you want to force the overwrite. Defaults to False.
            folderId (int, optional): the folderID in which the dashboard should be created. Defaults to None.
            message (str, optional): message to add when creating/updating the dashboard. Defaults to None.

        Returns:
            bool: [description]
        """
        jsonData = {
            "dashboard": dashboardJson,
            "folderId": folderId,
            "overwrite": overwrite
        }
        if message:
            jsonData["message"] = message

        r = self.session.post(self.url+"/api/dashboards/db", json=jsonData)
        if not r.ok:
            self.logger.error("Failed to push dashboard : %s", r.text)
            return False
        else:
            self.logger.debug("dashboard pushed successfully")
            return True
