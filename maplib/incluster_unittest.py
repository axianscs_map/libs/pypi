import maplib
import time
from random import randint
import threading
import os

logger = maplib.Logger("unittest")
logger.info("maplib version %s",maplib.__version__)

def test_mapvault():
    logger.info("testing vault")
    v = maplib.Vault()
    assert v.authenticate() == True


def test_mapinflux():
    logger.info("testing influx")

    i = maplib.Influx()
    rng = randint(0, 1000)
    assert isinstance(i.ping(), str)
    assert isinstance(i.get_list_database(), list)
    i.push([{"measurement": "unittest", "tags": {
           "rng": rng}, "fields": {"success": 1}}])
    r = i.query(
        'SELECT mean("success") FROM "unittest" WHERE ("rng" = \''+str(rng)+'\')')
    assert len(list(r)) >= 1

    # check rp through env var
    os.environ["MAPLIB_INFLUX_RETENTION_POLICY"] = "One_Day"
    second_influx = maplib.Influx()
    assert second_influx.retention_policy == os.getenv(
        "MAPLIB_INFLUX_RETENTION_POLICY")

    # now we should be in the other ret pol
    second_influx.push([{"measurement": "unittest", "tags": {
                       "rng": rng}, "fields": {"success": 1}}])
    r = second_influx.query(
        'SELECT mean("success") FROM "One_Day"."unittest" WHERE ("rng" = \''+str(rng)+'\')')
    assert len(list(r)) >= 1

    # check rp through env var if it's not correct
    os.environ["MAPLIB_INFLUX_RETENTION_POLICY"] = "rgzeregzreg"
    second_influx = maplib.Influx()
    assert second_influx.retention_policy == None


def test_hasura():
    logger.info("testing hasura in env var mode")
    os.environ["MAPLIB_HASURA_URL"] = "http://hasura-dev/v1/graphql"
    os.environ["MAPLIB_HASURA_TOKEN"] = "arandomtoken"

    # test a simple query
    ha = maplib.Hasura()
    query = "query test {dcim_device {name ip_address{address} status}}"
    rep = ha.query(query)
    assert isinstance(rep,dict)

    # auto mod
    logger.info("testing hasura in auto mode")
    del os.environ["MAPLIB_HASURA_TOKEN"]
    ha = maplib.Hasura()
    rep = ha.query(query)
    assert isinstance(rep,dict)

    # auto but with no token
    logger.info("testing hasura in auto mode with no token")
    ha = maplib.Hasura(secret_path="secret/hasura/notoken")
    rep = ha.query(query)
    assert rep is None


    # malformed queries
    # We should expect to fail during the parsing, catch it and return None
    
    logger.info("testing hasura with bad queries")
    malformed_query = "query test {dcim_device {name ip_address{addr"
    badrep = ha.query(malformed_query)
    assert badrep is None
    bad_query = "query test {dcim_device {name ip_address{address status}}"
    badrep = ha.query(bad_query)
    assert badrep is None

    # bad endpoint
    logger.info("testing hasura with bad endpoint")
    os.environ["MAPLIB_HASURA_URL"] = "http://hasura-does-not-exists/v1/graphql"
    os.environ["MAPLIB_HASURA_TOKEN"] = "arandomtoken"
    # test a simple query
    ha2 = maplib.Hasura()
    rep = ha2.query(query) # we should expect to fail during the request, catch it and return None
    assert rep == None


def test_mapminio():
    logger.info("testing minio")

    m = maplib.Minio()
    assert isinstance(m.list_buckets(), list)


def thread_worker(_id):
    n = maplib.Netbox()
    l = maplib.Logger("thr-{}".format(_id))
    l.info("starting !")
    n.logger.info("worker %s, this should be printed only once", _id)
    time.sleep(2)


def test_log():
    logger.info("testing log")
    threads = []
    for i in range(3):
        thr = threading.Thread(target=thread_worker, args=(i,))
        thr.start()
        threads.append(thr)

    while any([x.is_alive() for x in threads]):
        time.sleep(1)

    mylog = maplib.Logger("logtest")
    mylog.debug("Debug time !")
    mylog.info("Just a quick note")
    mylog.warning("We're reaching a limit..")
    mylog.error("Something's definitely not right")
    mylog.critical("If we're here, that's a problem.")


def test_mapnetbox():
    logger.info("testing netbox")

    n = maplib.Netbox()
    devicesList = n.retrieve_hosts_with_tags("ping")
    assert isinstance(devicesList, dict)
    # n.create_manufacturer("unittest")
    assert "unittest" in n.get_manufacturers()


def test_comments():
    logger.info("testing netbox comments")
    n = maplib.Netbox()

    n.nb.dcim.devices.create(
        name="unittest", status="active", site=1, device_type=1, device_role=1)

    for i in range(12):
        n.add_comment_to_device("unittest", "this is a test "+str(i))
    u = n.find_device_by_name("unittest")[0]

    print(dict(u)["comments"])

    u.delete()


def test_warranty():
    logger.info("testing netbox warranty")
    n = maplib.Netbox()

    logger.info("creating 2")
    n.nb.dcim.devices.create(name="unittest2", status="active",
                             site=1, device_type=1, device_role=1, tags=["unittest"])
    u2 = n.nb.dcim.devices.get(name="unittest2")
    print(dict(u2))

    logger.info("creating 3")
    n.nb.dcim.devices.create(name="unittest3", status="active", site=1,
                             device_type=1, device_role=1, serial="23456789", tags=["unittest"])
    u3 = n.nb.dcim.devices.get(name="unittest3")
    print(dict(u3))

    l = n.retrieve_hosts_with_tags("unittest")
    print(l)

    u3.delete()
    u2.delete()


def test_vms():
    logger.info("testings netbox VMs")
    n = maplib.Netbox()
    # create a vm with a tag
    print([x["name"] for x in n.retrieve_vms_with_tags("ping")])
    # retrieve hosts
    print(n.retrieve_all_with_tags("ping"))
    # we should have the vm


def test_grafana():
    logger.info("testing grafana")

    # failed password
    fg = maplib.Grafana(False,username="lolilol",password="nope")
    assert fg.username == "lolilol"

    # env var setup
    os.environ["MAPLIB_GRAFANA_PASSWORD"] = "envvarpassword"
    eg = maplib.Grafana()
    assert eg.password == "envvarpassword"
    
    del os.environ["MAPLIB_GRAFANA_PASSWORD"] 

    g = maplib.Grafana()
    # list the dash
    print(g.list_dashboards().keys())
    assert not g.dashboard_exists("unittest_dash")

    # create a tmp one
    dash = { 
        "id": None,
        "uid": None,
        "title": "unittest_dash",
        "tags": [ "templated" ],
        "timezone": "browser",
        "schemaVersion": 16,
        "version": 0,
        "refresh": "25s"
    }
    assert g.push_dashboard(dash,True)

    # check for it 
    assert g.dashboard_exists("unittest_dash")

    # check with the timed version
    assert g.cached_dashboard_exists("unittest_dash")

    # delete it
    g.delete_dashboard("unittest_dash")

    # check for it 
    assert not g.dashboard_exists("unittest_dash")
    assert g.cached_dashboard_exists("unittest_dash")
    
    # wait for the cache to be over
    time.sleep(60)
    assert not g.cached_dashboard_exists("unittest_dash")
