import setuptools


setuptools.setup(
    name="maplib",
    version="0.9.8",
    author="k0rventen",
    description="a wrapper for map services",
    python_requires='>=3.7', # minimum for async, datetime.datetime.fromisoformat(), and also because its 2021
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
    install_requires=["requests==2.25.1","hvac==0.10.6","pynetbox==5.3.1","minio==7.0.1",'influxdb==5.3.1',"gql==2.0.0"],
    extras_require = {'sentry': ['sentry-sdk']}
    )
