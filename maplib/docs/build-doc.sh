echo "cleaning up"
make clean

echo "converting readme to rst"
pandoc -f markdown -t rst -o readme.rst ../../README.md
pandoc -f markdown -t rst -o contributing.rst ../../CONTRIBUTING.md

echo "code report"
pylint -d C0301,C0303 -f text -r y ../maplib/*.py | awk  '/Report/,/EOF/' > pylint_report.rst

echo "building html"
make html

# if this is a ci job
[[ $1 = "ci" ]] && echo "moving build folder to /public" && mv _build/html ../../public

