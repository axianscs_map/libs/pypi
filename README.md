## maplib

__maplib__ is a wrapper for services in MAP


![pythonshield](https://img.shields.io/badge/python->=3.7-blue)
![doc](https://img.shields.io/badge/readthedoc-here-blue)

![stableshield](https://img.shields.io/badge/stable-0.9.7-green)
![devshield](https://img.shields.io/badge/dev-0.9.8rc4-red)

![easter1](https://img.shields.io/badge/not_a_bug-a_feature-purple)
![easter2](https://img.shields.io/badge/compatible-windows98-orange)

It provides autoconfigured modules for the following services available in MAP: 

- influx
- vault
- netbox
- loki 
- minio
- hasura (since 0.9.7)
- grafana (since 0.9.8)

And a logging module for standardized logging.


## Installation


If you want the stable version :

```sh
pip3 install --upgrade --extra-index-url https://gitlab.com/api/v4/projects/22719162/packages/pypi/simple maplib
```

Or the dev version : 

```sh
pip3 install --upgrade --pre --extra-index-url https://gitlab.com/api/v4/projects/22719162/packages/pypi/simple maplib
```

The `dev` version is usually stable. It might contain bugs for features developed in the given serie, but untouched modules are safe to use.


## Configuration

There are various methods for configuring __maplib__.

Either through code when things are not expected to change, or through environment variables. You'll find relevant information about the arguments you can pass to each module, or the environment variable it accepts in the documentation of that particular module.

## privileged mode

Vault works with roles, and policies attached to these roles.
Currently 2 roles are defined in maplib, `microservice` and `privileged`.

They are tied to specific serviceAccount in Kubernetes. 

It means that to be able to have the `privileged` role in vault, your pod will need the correct serviceAccount token.

If you are indeed a privileged pod, with the correct serviceAccount, you'll need to tell maplib about it, by using the
`.privileged()` method.

```python
import maplib

# i want to be privileged (and i have the associated serviceAccount to prove it)
maplib.privileged()
```


## Code examples

Here are a few snippets to get you started with each module. For a more in-depth usage, refer yourself to the code documentation or look through the existing microservices !

### maplog

```python
import maplib

logger = maplib.Logger("myapp")
logger.info("hello")
```

### mapinflux

```python
import maplib

i = maplib.Influx()
p = ("measurement":"ping","tags":{"ip":"10.0.1.2"},"fields":{"avg":0.027639,"min":0.01373,"max":0.28983})
i.push(p)
```

### maploki

```python
import maplib

l = maplib.Loki("myapp")
l.info("a line pushed to loki")
l.error("an error line pushed to loki")
```

### mapminio

```python
import maplib

m = maplib.Minio()
m.create_bucket("files")
m.upload_file("./file.txt","files","file.txt")
```

### mapnetbox

```python
import maplib

n = maplib.Netbox()
t = n.retrieve_hosts_with_tag("ping")
```
### maphasura

```python
import maplib

h = maplib.Hasura()
rep = h.query("query q {dcim_device{name}}")
```
### mapvault

```python
import maplib

v = maplib.Vault()
s = v.read_secret("ssh/generic")
```

### mapgrafana

```python
import maplib

dash = { 
    "id": None,
    "uid": None,
    "title": "my dash",
    "timezone": "browser",
    "schemaVersion": 16,
    "version": 0,
    "refresh": "25s"
}

g = maplib.Grafana()
if not g.cached_dashboard_exists("my-ms"):
    g.push_dashboard(dash)
```