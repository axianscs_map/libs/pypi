# Contribute to maplib

## General workflow

- `master` is the main branch where stable code is merged into.
- branches `vX.X.X` are specific to each version, and are used for development.
- `tags` on master will build maplib's wheel and documentation and push that to our pypi.

Main guidelines : 
- When working on the next version of maplib, work on a specific branch.
- Always use a different version schema in your `setup.py` to avoid potential conflicts.
- Wait for approval before merging to master.

## Bug 
If you discover a bug or a mistake, open an either an issue or create a branch with the fix.

## Feature 
For new features, include the modification in the next version's branch.
